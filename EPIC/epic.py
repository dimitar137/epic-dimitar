import os
import EPIC
from EPIC import gui
from EPIC.utils import io_tools, plots, convergence

def filtered_listdir(folder, ext, *action_args, action=None):
    filelist = [f for f in os.listdir(folder) if os.path.splitext(f)[1] == ext]
    if action is None:
        return filelist
    for f in filelist:
        action(os.path.join(folder, f), *action_args)

def analyze(sim, args=None, external_texts={}, show_bar=True):
    if args.convergence: #and if GelmanRubin
        setattr(args, 'burn_in', None)
    sim.analyze_chains(args, stop_at=args.stop_at,
            external_texts=external_texts, show_bar=show_bar)
    if args.convergence:
        _, hatRp = sim.Gelman_Rubin(args)
        from pprint import pprint
        pprint(hatRp-1)
    if args.plot:
        #setattr(args, 'kde', True)
        plots.make_plots([sim], args=args)

def burst(sim, parser, args=None):
    import shutil
    import configparser

    # None is a placeholder for the ini argument
    analyze_args = ['analyze', sim.working_dir, '--use-chain', None,
            '--dont-plot', '--bins', str(args.bins)]

    plot_args = ['plot', None, '--plot-prefix', args.plot_prefix,
            '--no-best-fit-marks']
    if args.kde:
        analyze_args.append('--kde')
        plot_args.append('--kde')
    if args.png:
        plot_args.append('--png')
    if args.use_tex:
        plot_args.append('--use-tex')
        
    # backup previous result
    burst_list = []
    orig_copy_ini = os.path.split(sim.analysis.ini_file)[1]
    burst_dir = io_tools.pasta(sim.working_dir, 'burst', clear=True)
    print('This may take some time...')
    use_chain = args.use_chain or list(range(len(sim.chains)))
    for chain in use_chain:
        analyze_args[3] = str(chain)
        analyze(sim, args=parser.parse_args(analyze_args), show_bar=False)
        this_chain_dir = io_tools.pasta(burst_dir, 'chain_%i' % chain)
        burst_list.append(this_chain_dir)
        loc = sim.plot_location() # stop_at = 
        dest = os.path.join(this_chain_dir,
            os.path.split(loc.rstrip(os.path.sep))[1])
        shutil.copytree(loc, dest)
        filtered_listdir(dest, '.pdf', action=os.remove)
        filtered_listdir(sim.working_dir, '.ini', this_chain_dir,
                action=shutil.copy2)
        conf = configparser.ConfigParser()
        copy_ini = os.path.join(this_chain_dir, orig_copy_ini)
        conf.read(copy_ini)
        conf['analysis']['label'] = 'Chain %i' % chain
        with open(copy_ini, 'w+') as cfg:
            conf.write(cfg)
        print('Chain %i' % chain)
        
    print('Plotting...')
    plots.make_plots(
            [sim_objects.load_existing_simulation(each_ini, print_info=False) \
                    for each_ini in burst_list], 
            args=parser.parse_args(plot_args))
    print('Done.')

if __name__ == '__main__':
    import argparse
    #from multiprocessing import freeze_support
    #freeze_support()

    parser = argparse.ArgumentParser(
            description='''This program allows parameter inference based on
                    Bayesian Analysis for models in Cosmology.'''
                    )
    subparsers = parser.add_subparsers()

    # launch gui
    parser_gui = subparsers.add_parser('gui', help='''Command for launching
            EPIC graphical user interface.''')

    parser_gui.add_argument('--theme', default='clam', choices=['alt', 'clam',
        'classic', 'default'], help='''Theme option for ttk widgets.''')

    parser_gui.set_defaults(func=gui.launch_gui)

    # run command
    parser_run = subparsers.add_parser('run', help='''Main command for running
            MCMC simulations.''')

    parser_run.add_argument('ini', help='''A .ini file or the path for the
            simulation you want to resume.''')

    parser_run.add_argument('chains', nargs='?', type=int, help='''The number
            of chains to be used in a new simulation.''')

    parser_run.add_argument('steps', type=int, help='''An integer number of steps
            for each MCMC loop.''')

    parser_run.add_argument('-m', '--mode', default='MCMC', choices=['MCMC',],# 'PT'],
            help='''Choose standard MCMC or Parallel Tempering algorithm
            (absent in this version).''')
    
    parser_run.add_argument('-c', '--check-interval',
            action=io_tools.TimeString,
            default=EPIC._defaults_['check_interval'], help='''Option for MCMC mode
            only. The time interval after which the chains should be checked
            for convergence. Accepts the formats 1h, 180min, etc.  Default is
            two hours.''')
    
    parser_run.add_argument('--tolerance', type=float,
            default=EPIC._defaults_['tolerance'], help='''Option for MCMC mode only.
            The tolerance below which the chains will be considered converged
            and the simulation will stop.''')

    parser_run.add_argument('--GR-steps', type=int, default=20, 
            metavar='STEPS', help='''Number of steps along the chain
            for the Gelman-Rubin evaluation.''')
    
    parser_run.add_argument('--GR-last', type=int, default=3, help='''Option
            for how many of the last steps of the Gelman Rubin evaluation
            should be considered on check during run.''')

    parser_run.add_argument('--save-rejected', action='store_true',
            help='''Option to write rejected states of each chain to files.''')

    parser_run.add_argument('-b', '--bins', type=int, default=20,
            help='''Number of bins for histograms (when not using kde).''')

    parser_run.add_argument('--limit', type=int, default=None, help='''Option
            intended for PT mode (not present in this version of the code),
            which does not check convergence periodically, but can be used with
            MCMC mode. The simulation will stop by itself after reaching this
            many steps. If not specified, it can only be terminated by the
            user.''') 

    #parser_run.add_argument('-s', '--swap', type=int, default=30,
    #        help='''Option for PT mode. The average number of steps after which
    #        a swap proposal should be made. Actual proposal rate will be random
    #        about this value.''')

    parser_run.add_argument('--proposal-covariance', default=None,
            help='''Option to start new simulation with a custom covariance in
            the proposal distribution.''')

    #parser_run.add_argument('--beta-scale', default='log', choices=['log', 'linear'],
    #        help='''Option for PT mode. The scale of the temperatures ladder
    #        for the parallel chains.''')

    #parser_run.add_argument('--beta-max', default=10, type=float, help='''Option for
    #        PT mode. The log_2 of the maximum temperature in the ladder.''')

    parser_run.add_argument('--sim-full-name', help='''Option for new simulations.
            Appends this given name rather than the date and time to the mode
            in the folder name.''')

    parser_run.add_argument('--sim-tag', help='''Option for new simulations.
            Preppends this given tag or label to the beginning of the folder
            name.''')

    parser_run.add_argument('--alt-dir', help='''Custom path for saving simulation
            results. If not specified, a folder named simulations in the same
            directory as the ini file will be created (if not already existing)
            and used.''')

    parser_run.add_argument('--multi-start', action='store_true', 
            help='''Option to start chains not from random
            points but from default parameter values.''')

    parser_run.add_argument('--burn-in', type=int, help='''Optional burn-in
            size.''')
    
    parser_run.add_argument('--starting-point', nargs='*', type=float, help='''A
            common starting point for all chains can be given instead of
            sampling from the priors. May be useful for testing and debugging.
            Use one parameter value per argument.''')

    parser_run.add_argument('--adapt', nargs='+', type=int, 
            default=0, help='''Option for adaptation of chains. Receives up to
            three numbers: the first is the number of free random walk loops
            before adapting (FREE, default 0), the second is the number of
            loops for adaptation of the covariance matrix of the proposal
            distribution (ADAPT, default is the same as FREE) and the number of
            STEPS in adaptive loops (default will be the same number of steps
            in regular loops.''')

    parser_run.add_argument('--acceptance-limits', nargs=2, type=float,
            default=EPIC._defaults_['acceptance_limits'], metavar=('MIN', 'MAX'),
            help='''Defines the acceptable range for the values of acceptance
            rates. Any chains presenting acceptance rate outside this range
            will be discarded prior to checking convergence.''')

    parser_run.add_argument('--chi2', action='store_true', help='''Option for
            evaluating likelihoods as log L = - chi^2/2 only or using all
            factors from the complete Gaussian distribution, including
            determinant of covariance matrix.''')

    parser_run.add_argument('--accepts-default', action='store_true',
            help='''Option for allowing no specification of parameter space
            point and predictions and likelihood evaluation with default
            values.''')

    # analyze command
    parser_analyze = subparsers.add_parser('analyze', help='''This module loads
            the chains for viewing and optionally checking convergence.''')

    parser_analyze.add_argument('ini', help='''The directory with the
            simulation to be analyzed.''')

    parser_analyze.add_argument('-s', '--stop-at', type=int, help='''Option for
            analyzing a chain until a certain size.''')

    parser_analyze.add_argument('--burn-in', type=int, help='''Optional burn-in
            size.''')
    
    parser_analyze.add_argument('--GR-steps', type=int, default=20, 
            metavar='STEPS', help='''Number of steps along the chain
            for the Gelman-Rubin evaluation.''')
    
    parser_analyze.add_argument('--kde', action='store_true', help='''Option for
            producing smoothed visualizations of the data distribution with
            kernel density estimation rather than histograms.''')

    parser_analyze.add_argument('--dont-redo-kde', action='store_false',
            dest='redo_kde', default=True, help='''This options helps avoiding
            repetition of kernel density estimation for all parameters when you
            only need some (for example, to complete some previous run that
            failed).''')

    parser_analyze.add_argument('-c', '--convergence', action='store_true',
            help='''Flag to perform calculation of convergence along the entire
            chains with all the GR_steps.''')

    parser_analyze.add_argument('--sequences', action='store_true',
            help='''Option to generate sequence plots.''')

    parser_analyze.add_argument('--correlation-function', action='store_true',
            help='''Option to calculate chains correlations.''')

    parser_analyze.add_argument('--use-tex', action='store_true',
            help='''Option to use LaTeX in sequence and correlation function
            plots.''')

    parser_analyze.add_argument('--use-chain', nargs='+', type=int,
            default=None, help='''Option to consider only specific chains. Must
            be a list of integers, first chain is 0. This is not intended for
            use with convergence monitoring, only for plotting chains
            separately.''')

    parser_analyze.add_argument('-b', '--bins', type=int, default=EPIC._defaults_['bins'],
            help='''Number of bins for histograms (when not using kde).''')

    parser_analyze.add_argument('--thin', type=int, help='''Thinning factor for
            kernel density estimates. By default is None and the code
            evaluates the factor according to the sample size.''')

    parser_analyze.add_argument('--kde-shuffle', action='store_true',
            help='''Whether or not to shuffle the sample before applying the
            thinning factor if kernel density estimates.''')

    parser_analyze.add_argument('--dont-plot', action='store_false',
            dest='plot', default=True, help='''Turns off automatic call of plot
            after analyze.''')

    #parser_analyze.add_argument('--interpolate-evidence', action='store_true',
    #        help='''Option to interpolate in the evaluation of the log of the
    #        evidence in PT simulations.''')

    parser_analyze.set_defaults(func=analyze)

    # burst command
    parser_burst = subparsers.add_parser('burst', help='''This command uses
            analyze and --use-chain to generate plots of individual chains from
            a single simulation.''')

    parser_burst.add_argument('ini', help='''Directory with the simulation
            results to analyze.''')

    parser_burst.add_argument('--use-chain', nargs='+', type=int,
            default=None, help='''Option to specify chains to be analyzed.''')

    parser_burst.add_argument('--kde', action='store_true', help='''Option for
            producing smoothed visualizations of the data distribution with
            kernel density estimation rather than histograms.''')

    parser_burst.add_argument('-b', '--bins', type=int, default=EPIC._defaults_['bins'],
            help='''Number of bins for histograms (when not using kde).''')

    parser_burst.add_argument('--plot-prefix', default='burst_plot', 
            help='''Custom prefix for the name of pdf files, useful for when
            plotting multiple analyses together and you do not want to
            overwrite original results.''')

    parser_burst.add_argument('--png', action='store_true', help='''Option to
            save plots in png besides pdf (always saved).''')

    parser_burst.add_argument('--use-tex', action='store_true', help='''Option
            to make nice plots using LaTeX.''')

    parser_burst.set_defaults(func=burst)

    # plot command
    parser_plot = subparsers.add_parser('plot', help='''Utility for producing
            plots with the results of analyze. This is called by analyze by
            default but can be run independently.''')

    parser_plot.add_argument('ini', nargs='+', help='''The directory with the
            analysis results to be plotted.''')

    parser_plot.add_argument('-s', '--stop-at', type=int, help='''Option for
            ploting a chain until a certain size.''')

    parser_plot.add_argument('--fmt', type=int, default=5, help='''Number of
            decimal places for displaying numerical results.''')

    parser_plot.add_argument('--kde', action='store_true', help='''Option for
            producing smoothed visualizations of the data distribution with
            kernel density estimation rather than histograms.''')

    parser_plot.add_argument('--use-tex', action='store_true', help='''Option
            to make nice plots using LaTeX.''')

    parser_plot.add_argument('--font-size', type=int, default=8, help='''Font
            size for plots.''')

    parser_plot.add_argument('--levels', nargs='+', type=int,
            default=EPIC._defaults_['sigma_levels'], choices=[1, 2, 3, 4, 5],
            help='''Sigma confidence levels to show in plots.''')

    parser_plot.add_argument('--exclude', nargs='+', help='''Option to exclude
            any parameter from triangle plots. Use "nuisance" to exclude all
            nuisance parameters.''')

    parser_plot.add_argument('--show-hist', action='store_true', help='''Option
            to show histograms together with kde when using kde.''')

    parser_plot.add_argument('--show-gaussian-fits', action='store_true',
            help='''Option to show gaussian fits together with histograms or
            kde curves.''')

    parser_plot.add_argument('--no-best-fit-marks', action='store_false',
            dest='mark_best_fit', default=True, help='''Option to suppress plot
            of best fit points.''')

    parser_plot.add_argument('--png', action='store_true', help='''Option to
            save plots in png besides pdf (always saved).''')

    parser_plot.add_argument('--plot-prefix', help='''Custom prefix for the
            name of pdf files, useful for when plotting multiple analyses
            together and you do not want to overwrite original results.''')

    parser_plot.add_argument('--font', default='Computer Modern', 
            choices=list(io_tools.pdf_fonts.keys()),
            help='''Option to choose font for plot with tex.''')

    parser_plot.add_argument('--detect', default=None, help='''Option to
            calculate how many sigmas of detection (from zero) of a given
            parameter.''')

    parser_plot.add_argument('--no-auto-factors', action='store_true',
            help='''Option to not detect scales and set factors (power of 10)
            automatically in plots.''')

    parser_plot.add_argument('--no-custom-ticks', action='store_true',
            default=False, help='''Option to disable use of custom ticks 
            from ini file.''')
    
    parser_plot.add_argument('--no-auto-range', action='store_true',
            default=False, help='''Option to disable use of automatic ranges
            from final distributions. If disabled, will use priors.''')

    parser_plot.add_argument('--no-units', action='store_true',
            default=False, help='''Option to omit units in the plot axes labels
            when they are given in the .ini file.''')

    parser_plot.add_argument('--no-title-above', action='store_false',
            default=True, dest='title_above', help='''Option to print plot name
            inside frame rather than above.''') 

    parser_plot.add_argument('--title-pos', default='upper right',
            help='''Alignment of title inside frame when --no-title-above is
            used.''')

    parser_plot.add_argument('--color', default='C0', help='''Main color
            option for plots of a single analysis. Use any matpotlib color.''')

    parser_plot.add_argument('--color-scheme', choices=EPIC.color_options,
        default='tableau', help='''Color palette option for plots with multiple
        analysis. Options are from matplotlib._color_data.  XKCD_COLORS and
        CSS4_COLORS are dictionaries containing 949 and 148 colors each one,
        respectively. Because they are not ordered dictionaries, each time you
        plot using them you will get a different color scheme with random
        colors, so have fun! The schemes with -light, -dark, etc, just filter
        the lists returning only the colors that have that characteristic in their
        names.''')

    parser_plot.add_argument('--style', choices=EPIC.pyplot_grid_styles,
            default='default', help='''Matplotlib style option for plots.''')
    
    parser_plot.set_defaults(func=plots.make_plots)

    parser_monitor = subparsers.add_parser('monitor', help='''Utility for
            visualizing graphics for monitoring convergence.''')

    parser_monitor.add_argument('ini', nargs='+', help='''The existing
            simulation(s) directory.''')

    parser_monitor.add_argument('--use-tex', action='store_true',
            help='''Option to use LaTeX in plots.''')

    parser_monitor.add_argument('--png', action='store_true', help='''Option to
            save plots to png besides pdf.''')

    parser_monitor.set_defaults(func=convergence.monitor)

    #parser.add_argument('rargs', nargs=argparse.REMAINDER, help='''Collect
    #        argments for analyze after running MCMC.''')
    

    import sys
    sysargs = list(sys.argv)
    sysargs.pop(0)
    if len(sysargs) == 0:
        sysargs.append('gui')
    args = parser.parse_args(sysargs)
    #from pprint import pprint
    #pprint(vars(args))

    if hasattr(args, 'func'):
        if hasattr(args, 'ini'):
            from EPIC import sim_objects
            if isinstance(args.ini, list):
                sim = [sim_objects.load_existing_simulation(each_ini, 
                    print_info=False) for each_ini in args.ini]
                for simulation in sim:
                    print(simulation.working_dir)
            else:
                sim = sim_objects.load_existing_simulation(args.ini, 
                        print_info=False)
                # for burst, analyze and plot with 1 ini argument
            if args.func == burst:
                args.func(sim, parser, args=args)
            else:
                args.func(sim, args=args)
        else:
            # for gui
            args.func(args, parser_run, parser_analyze, parser_plot,
                    parser_monitor, analyze)
    else:
        assert not isinstance(args.ini, list)
        from EPIC import sim_objects
        # in which case we would 
        # return [sim_objects.load_existing_simulation(each_ini) for each_ini
        # in args.ini]
        # but only analyze and plot commands should use lists, and they both
        # have attribute 'func'

        if os.path.isfile(args.ini):
            working_dir, analysis = sim_objects.create_new_simulation(args)
            sim = sim_objects.load_existing_simulation(working_dir, analysis)
        else:
            sim = sim_objects.load_existing_simulation(args.ini)

        if sim.start(args):
            rargs = parser_analyze.parse_args(
                    args=[sim.working_dir, '--convergence', '--kde']
                    )
            analyze(sim, args=rargs)

