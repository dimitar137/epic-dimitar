import os
os.chdir('/mnt/c/Users/rafae/repo/epicas/EPIC')
import EPIC.cosmology.cosmic_objects as cosmo
import pytest

@pytest.fixture
def lcdm():
    return cosmo.CosmologicalSetup('lcdm', derived='lambda', physical=False)

def test_flat_prior(lcdm):
    lcdm.species['cdm'].density_parameter.set_prior(0, 0.5)
    lcdm.HubbleParameter.set_prior(0.7, 0.02, distribution='Gaussian')
    assert hasattr(lcdm.species['cdm'].density_parameter.prior, 'vmin')
    assert hasattr(lcdm.species['cdm'].density_parameter.prior, 'vmax')
    assert hasattr(lcdm.HubbleParameter.prior, 'sigma')
    assert hasattr(lcdm.HubbleParameter.prior, 'mu')

def test_default_values_of_parameters(lcdm):
    assert lcdm.HubbleParameter.default == 68
    assert lcdm.species['cdm'].density_parameter.default == 0.3

