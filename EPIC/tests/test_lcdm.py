import os
os.chdir('/mnt/c/Users/rafae/repo/epicas/EPIC')
import EPIC.cosmology.cosmic_objects as cosmo
import pytest

def test_basic_lcdm():
    ''' Basic two-fluid lambda-cdm model, with physical densities and lambda as
    a derived parameter ( Omega_L = 1 - Omega_cdm ) '''
    model = cosmo.CosmologicalSetup('lcdm', derived='lambda')
    assert set(model.species) == set(['lambda', 'cdm'])
    assert isinstance(model.species['cdm'].density_parameter, \
            cosmo.DensityParameter)
    assert isinstance(model.species['lambda'].density_parameter, \
            cosmo.DerivedParameter)
    assert model.species['lambda'].EoS.cosmological_constant
    assert model.species['cdm'].EoS.pressureless
    assert set([par.label for par in model.parameters]) == set(['Och2', 'Olh2', 'h'])

def test_lcdm_baryons():
    ''' LCDM model with optional amount of baryons; physical densities, lambda
    as derived'''
    model = cosmo.CosmologicalSetup('lcdm', 
            optional_species=['baryons'],
            derived='lambda')
    assert set(model.species) == set(['lambda', 'cdm', 'baryons'])
    assert isinstance(model.species['baryons'].density_parameter, \
            cosmo.DensityParameter)
    assert isinstance(model.species['cdm'].density_parameter, \
            cosmo.DensityParameter)
    assert isinstance(model.species['lambda'].density_parameter, \
            cosmo.DerivedParameter)
    assert model.species['lambda'].EoS.cosmological_constant
    assert model.species['baryons'].EoS.pressureless
    assert model.species['cdm'].EoS.pressureless
    assert set([par.label for par in model.parameters]) == set(['Och2', 'Obh2', 'Olh2', 'h'])

@pytest.mark.parametrize("bar_presence", [
    (['baryons'],),
    ([],),
])
def test_lcdm_baryons_matter(bar_presence):
    ''' LCDM model with and without passing optional amount of baryons but then
    replacing with matter; physical densities, lambda as derived'''
    model = cosmo.CosmologicalSetup('lcdm', 
            optional_species=bar_presence,
            combined_species=['matter'],
            derived='lambda')
    assert set(model.species) == set(['lambda', 'matter'])
    assert isinstance(model.species['matter'].density_parameter, \
            cosmo.DensityParameter)
    assert isinstance(model.species['lambda'].density_parameter, \
            cosmo.DerivedParameter)
    assert model.species['lambda'].EoS.cosmological_constant
    assert model.species['matter'].EoS.pressureless
    assert set([par.label for par in model.parameters]) == set(['Omh2', 'Olh2', 'h'])

def test_lcdm_photons():
    ''' LCDM model passing optional amount of photons; physical densities,
    lambda as derived'''
    model = cosmo.CosmologicalSetup('lcdm', 
            optional_species=['photons'],
            derived='lambda')
    assert set(model.species) == set(['lambda', 'cdm', 'photons'])
    assert isinstance(model.species['cdm'].density_parameter, \
            cosmo.DensityParameter)
    assert isinstance(model.species['lambda'].density_parameter, \
            cosmo.DerivedParameter)
    assert isinstance(model.species['photons'].density_parameter, \
            cosmo.DensityParameter)
    assert model.species['lambda'].EoS.cosmological_constant
    assert model.species['cdm'].EoS.pressureless
    assert model.species['photons'].EoS.constant_EoS
    assert set([par.label for par in model.parameters]) == \
            set(['Och2', 'Olh2', 'Ogh2', 'h'])

@pytest.mark.parametrize("photon_presence", [
    (['photons'],),
    ([],),
])
def test_lcdm_photons_rad(photon_presence):
    ''' LCDM model with and without passing optional amount of photons but then
    replacing with radiation; physical densities, lambda as derived'''
    model = cosmo.CosmologicalSetup('lcdm', 
            optional_species=photon_presence,
            combined_species=['radiation'],
            derived='lambda')
    assert set(model.species) == set(['lambda', 'cdm', 'radiation'])
    assert isinstance(model.species['cdm'].density_parameter, \
            cosmo.DensityParameter)
    assert isinstance(model.species['lambda'].density_parameter, \
            cosmo.DerivedParameter)
    assert isinstance(model.species['radiation'].density_parameter, \
            cosmo.DensityParameter)
    assert model.species['lambda'].EoS.cosmological_constant
    assert model.species['cdm'].EoS.pressureless
    assert model.species['radiation'].EoS.constant_EoS
    assert set([par.label for par in model.parameters]) == set(['Och2', 'Olh2', 'Orh2', 'h'])

def test_lcdm_baryons_photons_neutrinos():
    ''' LCDM model with all possible optionals; physical densities, lambda as
    derived'''
    model = cosmo.CosmologicalSetup('lcdm', 
            optional_species=['photons', 'baryons', 'neutrinos'],
            #combined_species=['radiation', 'matter'],
            derived='lambda')
    assert set(model.species) == set([
        'lambda', 'cdm', 'photons', 'baryons', 'neutrinos'])
    assert isinstance(model.species['cdm'].density_parameter, \
            cosmo.DensityParameter)
    assert isinstance(model.species['baryons'].density_parameter, \
            cosmo.DensityParameter)
    assert isinstance(model.species['lambda'].density_parameter, \
            cosmo.DerivedParameter)
    assert isinstance(model.species['photons'].density_parameter, \
            cosmo.DensityParameter)
    assert isinstance(model.species['neutrinos'].density_parameter, \
            cosmo.DensityParameter)
    assert model.species['lambda'].EoS.cosmological_constant
    assert model.species['cdm'].EoS.pressureless
    assert model.species['baryons'].EoS.pressureless
    assert model.species['photons'].EoS.constant_EoS
    assert model.species['neutrinos'].EoS.constant_EoS
    assert set([par.label for par in model.parameters]) == set(['Och2', 'Obh2', 'Olh2', 'Ogh2', 'Onh2', 'h'])

def test_lcdm_baryons_photons_neutrinos_nonphys():
    ''' LCDM model with all possible optionals; nonphysical densities, lambda as
    free parameter'''
    model = cosmo.CosmologicalSetup('lcdm', 
            optional_species=['photons', 'baryons', 'neutrinos'],
            #combined_species=['radiation', 'matter'],
            physical=False)
    assert set(model.species) == set([
        'lambda', 'cdm', 'photons', 'baryons', 'neutrinos'])
    assert isinstance(model.species['cdm'].density_parameter, \
            cosmo.DensityParameter)
    assert isinstance(model.species['baryons'].density_parameter, \
            cosmo.DensityParameter)
    assert isinstance(model.species['lambda'].density_parameter, \
            cosmo.DensityParameter)
    assert isinstance(model.species['photons'].density_parameter, \
            cosmo.DensityParameter)
    assert isinstance(model.species['neutrinos'].density_parameter, \
            cosmo.DensityParameter)
    assert model.species['lambda'].EoS.cosmological_constant
    assert model.species['cdm'].EoS.pressureless
    assert model.species['baryons'].EoS.pressureless
    assert model.species['photons'].EoS.constant_EoS
    assert model.species['neutrinos'].EoS.constant_EoS
    assert set([par.label for par in model.parameters]) == set(['Oc0', 'Ob0', 'Ol0', 'Og0', 'On0', 'H0'])

def test_lcdm_baryons_photons_neutrinos_rad_matter():
    ''' LCDM model with all possible optionals; physical densities, lambda as
    derived'''
    model = cosmo.CosmologicalSetup('lcdm', 
            optional_species=['photons', 'baryons', 'neutrinos'],
            combined_species=['radiation', 'matter'],
            derived='lambda')
    assert set(model.species) == set(['lambda', 'matter', 'radiation'])
    assert isinstance(model.species['matter'].density_parameter, \
            cosmo.DensityParameter)
    assert isinstance(model.species['lambda'].density_parameter, \
            cosmo.DerivedParameter)
    assert isinstance(model.species['radiation'].density_parameter, \
            cosmo.DensityParameter)
    assert model.species['lambda'].EoS.cosmological_constant
    assert model.species['matter'].EoS.pressureless
    assert model.species['radiation'].EoS.constant_EoS
    assert set([par.label for par in model.parameters]) == set(['Omh2', 'Olh2', 'Orh2', 'h'])

def test_lcdm_baryons_photons_neutrinos_rad_matter_nonphys():
    ''' LCDM model with all possible optionals; nonphysical densities, lambda as
    free parameter'''
    model = cosmo.CosmologicalSetup('lcdm', 
            optional_species=['photons', 'baryons', 'neutrinos'],
            combined_species=['radiation', 'matter'],
            physical=False)
    assert set(model.species) == set(['lambda', 'matter', 'radiation'])
    assert isinstance(model.species['matter'].density_parameter, \
            cosmo.DensityParameter)
    assert isinstance(model.species['lambda'].density_parameter, \
            cosmo.DensityParameter)
    assert isinstance(model.species['radiation'].density_parameter, \
            cosmo.DensityParameter)
    assert model.species['lambda'].EoS.cosmological_constant
    assert model.species['matter'].EoS.pressureless
    assert model.species['radiation'].EoS.constant_EoS
    assert set([par.label for par in model.parameters]) == set(['Om0', 'Ol0', 'Or0', 'H0'])
