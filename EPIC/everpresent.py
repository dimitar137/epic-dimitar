from EPIC.cosmology import cosmic_objects as cosmo
"""LCDM = cosmo.CosmologicalSetup(
    'lcdm',
    optional_species = ['baryons', 'radiation'],
    combined_species = ['matter'],
    derived = 'lambda'
)"""
EVER = cosmo.CosmologicalSetup(
    'everde',
    optional_species = ['baryons', 'radiation', 'de_ever'],
    combined_species = ['matter'],
    physical=False,
    derived = 'lambda'
)
EVER.solve_background(accepts_default = True)
from EPIC.cosmology import observations
from EPIC.utils.statistics import Analysis
datasets = observations.choose_from_datasets({
    'Hz': 'cosmic_chronometers',
    'H0': 'HST_local_H0',
    'SNeIa': 'JLA_simplified',
    })
priors = {
    'Om0' : [0, 0.5],
    'Od0' : [0, 0.5],
    'H0' : [50, 90],
    'M' : [-0.3, 0.3],
    'T_CMB' : [2.27,2.27255],
    }
analysis = Analysis(datasets, EVER, priors)
analysis.log_posterior(parameter_space={'Od0': 0.3,'Om0': 0.3, 'H0': 68, 'M': 0, 'T_CMB': 2.2755},chi2=True)

#%matplotlib inline
import matplotlib.pyplot as plt
import numpy as np
plt.rcParams['text.usetex'] = True
plt.rcParams['font.size'] = 16
plt.rcParams['figure.dpi'] = 144
def show_densities(model, **kwargs):
    from EPIC.cosmology import rho_critical_SI
    hubble = model.HubbleParameter.get_value(**kwargs)
    rho_cr0 = rho_critical_SI(
        hubble * (100 if model.physical_density_parameters else 1)
    )
    fig, ax = plt.subplots(1, 2)
    fig.set_size_inches(12, 4)
    for key in model.background_solution_Omegas.keys():
        print(key)
        ax[0].plot(model.a_range, model.background_solution_Omegas[key], lw=2)
        #ax[0].plot(model.a_range, model.background_solution_rhos[key], lw=2)
        ax[1].plot(model.a_range, rho_cr0 \
                   * (hubble**-2 if model.physical_density_parameters else 1)\
                   * model.background_solution_rhos[key], lw=2)
    ax[0].set_xscale('log')
    #ax[0].set_ylabel(r'$\Omega$')
    #ax[0].set_xlabel(r'$a$')
    ax[0].grid(which='both', linestyle=':')
    #ax[0].legend()
    ax[1].set_xscale('log')
    ax[1].set_yscale('log')
    #ax[1].set_ylabel(r'$\rho \, \left[ \rm{kg}/\rm{m}^3 \right]$')
    #ax[1].set_xlabel(r'$a$')
    ax[1].grid(which='both', linestyle=':')
    ax[1].legend(fontsize=14)
    fig.tight_layout()
#show_densities(EVER, accepts_default=True)
plt.semilogx(EVER.a_range,EVER.background_solution_rhos['de_ever'], lw=2)
plt.show()
#plt.savefig("ever-dens.pdf")
                   
