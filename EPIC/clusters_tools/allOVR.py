import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import numpy as np
#import scipy.stats as st
import os
import sys
from EPIC.utils import data_tools, statistics
from collections import OrderedDict
import pickle

plt.rc('text', usetex= True)
plt.rc('font', **{'family':'serif', 'serif':['Times']})

ovr_dict = {}
for dataset in ['OkabeSmithMantz', 'OkabeSmithMartino', 'OkabeSmithMaughan']:
    try:
        with open('dict_%s.p' % dataset, 'rb') as filehandler:
            ovr_dict[dataset] = pickle.load(filehandler)
    except:
        ovr_dir = os.path.join('..', 'cosmology', 'observational_data', 'NonVirializedClusters', dataset, 'OVR')
        ovr_files = os.listdir(ovr_dir)
        ovr_dict[dataset] = OrderedDict()

        fig, ax = plt.subplots(6, 5)#, sharex='col', sharey='row')
        fig.set_size_inches(3*5, 2*6)
        table = open('OVRtable_%s.txt' % dataset, 'w')
        for i, cluster in enumerate(ovr_files):
            #cluster = cluster.replace('RXJ1504', 'RXJ1504.1')
            jx, jy = divmod(i, 5)
            OVR = np.loadtxt(os.path.join(ovr_dir, cluster))
            sup, dens = statistics.make_kde([OVR,], thin=1)
            ax[jx][jy].hist(OVR, normed=True, bins=50, alpha=0.6)
            ax[jx][jy].plot(sup, dens, color='r', lw=0.5)
            xmode, xmin, xmax = statistics.fit_asymmetrical(None, kdefit_asym=(sup, dens))
            x0, xp, xm = data_tools.convert_points_errorbars(xmode, xmin, xmax, fmt=None)
            ovr_dict[dataset][cluster[:-4]] = [x0, xp, xm]
            ax[jx][jy].axvline(xmode, lw=0.5, color=(0.3, 0.3, 0.3))
            ax[jx][jy].axvline(xmin, lw=0.5, color=(0.6, 0.6, 0.6))
            ax[jx][jy].axvline(xmax, lw=0.5, color=(0.6, 0.6, 0.6))
            #or_xmode, or_xmin, or_xmax = statistics.fit_asymmetrical(OVR, nbins=50)
            #or_x0, or_xp, or_xm = data_tools.convert_points_errorbars(or_xmode,
            #   or_xmin, or_xmax, fmt=None)
            #ax[jx][jy].axvline(or_x0, lw=0.3, color=(0.8, 0., 0.8), ls='--')
            #ax[jx][jy].axvline(or_xmin, lw=0.3, color=(0.8, 0., 0.8), ls='--')
            #ax[jx][jy].axvline(or_xmax, lw=0.3, color=(0.8, 0., 0.8), ls='--')
            ax[jx][jy].tick_params(left='off', right='off')#, bottom='off', top='off')
            ax[jx][jy].set_yticklabels('')
            #ax[jx][jy].set_xticklabels('')
            value_string =  '$%.2f^{+%.2f}_{-%.2f}$' % \
                    data_tools.convert_points_errorbars(xmode, xmin, xmax, fmt=2)
            table.write('\t&\t'.join([cluster[:-4], value_string]) + '\\\\' + '\n')
            label = '$OVR = ' + value_string[1:]
            label += '\n' + cluster[:-4]
            handle = mpatches.Patch(linewidth=0, label=label)
            ax[jx][jy].legend(loc='upper left', handles=[handle,],
                    borderaxespad=0.2, handlelength=0, handletextpad=0,
                    frameon=False, fontsize=9)
            print("%2i" % (i+1), cluster[:-4], OVR.size)
        table.close()

        #while i <= 18:
        #    i += 1
        #    jx, jy = divmod(i, 5)
        #    fig.delaxes(ax[jx][jy])
        #    #ax[jx][jy].set_xticklabels('')
        #    #ax[jx][jy].set_yticklabels('')
        #    #ax[jx][jy].tick_params(left='off', right='off', bottom='off', top='off')

        fig.tight_layout()
        #fig.subplots_adjust(hspace=0,wspace=0)
        fig.savefig('OVR %s %s.pdf' % (dataset, OVR.size))
        with open('dict_%s.p' % dataset, 'wb') as filehandler:
            pickle.dump(ovr_dict[dataset], filehandler, 0)

size = 10000

plt.rcParams['axes.linewidth'] = 0.5
fig, ax = plt.subplots()
fig.set_size_inches(8, 15)
ax.grid(axis='both', which='both', ls='--', lw=0.5)
ax.axvline(-0.5, color='r', lw=0.5, zorder=1)
colors = ['r', 'g', 'b']
all_clusters = []
for dataset in ['OkabeSmithMantz', 'OkabeSmithMartino', 'OkabeSmithMaughan']:
    all_clusters += list(ovr_dict[dataset].keys())
all_clusters = list(set(all_clusters))
all_clusters = sorted(all_clusters)[::-1]

for i, dataset in enumerate(['OkabeSmithMantz', 'OkabeSmithMartino',
    'OkabeSmithMaughan']):
    c = colors[i]
    ax.errorbar(
            [ovr_dict[dataset][cl][0] if cl in ovr_dict[dataset] else np.nan for cl in all_clusters],
            np.array(list(range(len(all_clusters)))) + (i+1)*0.05 - 0.1,
            xerr=np.transpose([[ovr_dict[dataset][cl][2], ovr_dict[dataset][cl][1]] if cl in ovr_dict[dataset] else [np.nan, np.nan] for cl in all_clusters]),
            marker='o', mfc=c, mec=c, ecolor=c, linestyle='',
            elinewidth=1., markersize=4, capsize=3, zorder=2,
            label=dataset.replace('OkabeSmith', '')
            )
ax.legend()
ax.set_xlabel('OVR')
ax.set_yticks(list(range(len(all_clusters))))
ax.set_yticklabels(all_clusters)
fig.tight_layout()
fig.savefig('OVRerrorbars all %s.pdf' % size)
