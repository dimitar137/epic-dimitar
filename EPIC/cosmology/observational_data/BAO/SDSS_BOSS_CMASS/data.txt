# effective redshift 0.57
# the redshift is read from the first line. 
# do not touch it!
# Anderson et al., MNRAS 441 (2014) 24
1421    20
96.8    3.4
