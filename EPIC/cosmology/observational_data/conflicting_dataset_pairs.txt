# conflicting pairs of datasets. If you try to use them together you will get
# an annoying error message
SDSS_BOSS_CMASS , WiggleZ
SDSS_BOSS_LOWZ , WiggleZ
SDSS_BOSS_QuasarLyman , WiggleZ
SDSS_BOSS_consensus , WiggleZ
SDSS_BOSS_Lyalpha-Forests , WiggleZ
Planck2015_distances_LCDM , Planck2015_distances_wCDM
Planck2015_distances_LCDM , Planck2018_distances_wCDM
Planck2018_distances_LCDM , Planck2015_distances_LCDM
Planck2018_distances_LCDM , Planck2015_distances_wCDM
Planck2018_distances_LCDM , Planck2018_distances_wCDM
Planck2018_distances_wCDM , Planck2015_distances_wCDM
# Planck2015_distances_LCDM , Planck2015_distances_LCDM+Omega_k
# Planck2015_distances_wCDM , Planck2015_distances_LCDM+Omega_k
JLA_simplified , JLA_full
