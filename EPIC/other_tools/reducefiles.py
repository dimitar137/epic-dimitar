import os
import sys
from EPIC.utils import io_tools

def reducechains(wdir):

    nchains = io_tools.readint(os.path.join(wdir, 'nchains.txt'))
    nparams = io_tools.readint(os.path.join(wdir, 'nparams.txt'))

    for I in range(nchains):
        f = open(os.path.join(wdir,'PTchain%i.txt' % (I+1)), 'r')
        g = open(os.path.join(wdir,'reducedchain%i.txt' % (I+1)), 'w')
        
        nextline = f.readline()
        while nextline:
            line = str(nextline)
            lastline = list(line.strip('\n').split(', '))
            r = 1
            nextline = f.readline()
            while nextline.split(', ')[:nparams] == line.split(', ')[:nparams]:
                r += 1
                lastline = list(nextline.strip('\n').split(', '))
                nextline = f.readline()
                if not bool(nextline):  # termino do arquivo, nextline is an empty string
                    break
            g.write(', '.join(lastline + ['%i' % r]) + '\n')
        f.close()
        g.close()
        print('Finished reducing chain%i' % (I+1))

if __name__ == '__main__':
    reducechains(sys.argv[1])
