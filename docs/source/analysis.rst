
.. code:: ipython3

    >>> import EPIC.cosmology.cosmic_objects as cosmo
    >>> from EPIC.cosmology import observations
    >>> from EPIC.utils.statistics import Analysis
    
    >>> lcdm = cosmo.CosmologicalSetup(
    ...     'lcdm', physical=False, derived='lambda'
    ... )
    
    >>> datasets = observations.choose_from_datasets({
    ...     'Hz': 'cosmic_chronometers',
    ...     'H0': 'HST_local_H0',
    ...     'SNeIa': 'JLA_simplified',
    ... })
    
    >>> priors = {
    ...     'Oc0' : [0, 0.5],
    ...     'H0' : [50, 90],
    ...     'M' : [-0.3, 0.3],
    ... }
    >>> analysis = Analysis(datasets, lcdm, priors)
    >>> analysis.log_posterior(parameter_space={'Oc0': 0.3, 'H0': 68, 'M': 0}, chi2=True)
    (-38.37946644320705, -35.38373416965306)


