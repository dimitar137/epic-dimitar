.. _datasec:

The datasets
------------

Observations available for comparison with model predictions are
registered in the
``EPIC/cosmology/observational_data/available_observables.ini``. They
are separated in sections (``Hz``, ``H0``, ``SNeIa``, ``BAO`` and
``CMB``), which contain the name of the ``CosmologicalSetup``'s module for the
observable theoretical calculation (``predicting_function_name``). 
Each dataset goes below one of the sections, with the folder or text file
containing the data indicated.
The path is relative to the ``EPIC/cosmology/observational_data/`` folder.
If the folder name is the same as the observable
label it can be ommited. Besides, the ``Dataset`` subclasses are defined
at the beginning of the ``.ini`` file. Each of these classes has its own
methods for initialization and likelihood evaluation.

We choose the datasets by passing a dictionary with observables as keys and
datasets as values to the function ``choose_from_datasets`` from
``observations`` in ``EPIC.cosmology``:

.. code:: ipython3

    >>> from EPIC.cosmology import observations
    
    >>> datasets = observations.choose_from_datasets({
    ...     'Hz': 'cosmic_chronometers',
    ...     'H0': 'HST_local_H0',
    ...     'SNeIa': 'JLA_simplified',
    ...     'BAO': [
    ...         '6dF+SDSS_MGS', # z = 0.122
    ...         'SDSS_BOSS_CMASS', # z = 0.57
    ...         'SDSS_BOSS_LOWZ', # z = 0.32, 0.57
    ...         'SDSS_BOSS_QuasarLyman', # z = 2.36
    ...         'SDSS_BOSS_consensus', # z = [0.38, 0.51, 0.61]
    ...         'SDSS_BOSS_Lyalpha-Forests', # z = 2.33
    ...         #'WiggleZ', # z = [0.44, 0.6, 0.73]
    ...     ],
    ...     'CMB': 'Planck2015_distances_LCDM', 
    ... })

The ``WiggleZ`` dataset is available but is commented out because it is
correlated with the SDSS datasets and thus not supposed to be used together
with them.
Refer to the papers published by the authors of the observations for details.
Other incompatible combinations are defined in the
``conflicting_dataset_pairs.txt`` file in
``EPIC/cosmology/observational_data/``.
The code will check for these conflicts prior to proceeding with an analysis.
The returned flattened dictionary of Dataset objects will later be passed to a
MCMC analysis.
Now I describe briefly the datasets made available by the community.

Type Ia supernovae
^^^^^^^^^^^^^^^^^^

Two types of analyses can be made with the JLA catalogue.
One can either use the full likelihood (``JLA_full``) or a simplified version
based on 30 redshift bins (``JLA_simplified``).
Here we are using the binned data consisting of distance modulus estimates at
31 points (defining 30 bins of redshift).
If you want to use the full dataset (which makes the analysis much slower since
it involves three more nuisance parameters and requires the program to invert a
740 by 740 matrix at every iteration for the calculation of the JLA
likelihood), you need to download the covariance matrix data
(``covmat_v6.tgz``) from
`<http://supernovae.in2p3.fr/sdss_snls_jla/ReadMe.html>`_. The ``covmat``
folder must be extracted to the ``jla_likelihood_v6`` folder.
This is not included in EPIC because the data files are too big.

Either way, the data location is the same data folder ``jla_likelihood_v6``.
Note that the binned dataset introduces one nuisance parameter ``M``,
representing an overall shift in the absolute magnitudes, and the full dataset
introduces four nuisance parameters related to the light-curve parametrization.
See Betoule et al. (2014) [#Betoule2014]_ for more details.

CMB distance priors
^^^^^^^^^^^^^^^^^^^

Constraining models with temperature or polarization anisotropy amplitudes is
not currently implemented.
However, you can include the CMB distance priors from Planck2015 [#Huang2015]_
or the updated priors from Planck2018. [#Chen2018]_
The datasets consist of an acoustic scale :math:`l_A`, a shift parameter
:math:`R` and the physical density of baryons :math:`\Omega_{b0}h^2`.
You can choose between the data for :math:`\Lambda\text{CDM}` and 
:math:`w\text{CDM}` with either ``Planck2015_distances_LCDM``,
``Planck2015_distances_wCDM``, ``Planck2018_distances_LCDM``, or
``Planck2018_distances_wCDM``;
``Planck2015_distances_LCDM+Omega_k`` 
``Planck2018_distances_LCDM+Omega_k`` are also available for when curvature is
supported.

BAO data
^^^^^^^^

Measurements of the baryon acoustic scales from the Six Degree Field Galaxy
Survey (6dF) combined with the most recent data releases of Sloan Digital Sky
Survey (SDSS-MGS), [#Carter2018]_ the LOWZ and CMASS galaxy samples of the
Baryon Oscillation Spectroscopic Survey (BOSS-LOWZ and BOSS-CMASS),
[#Anderson2014]_ data from 
the Quasar-Lyman :math:`\alpha` cross-correlation, [#FontRibera2014]_ 
the distribution of the Lyman :math:`\alpha` forest in BOSS [#Bautista2017]_ 
and from the
WiggleZ Dark Energy Survey [#Kazin2014]_ are available in the ``BAO`` folder,
as well as the latest consensus from the completed SDSS-III BOSS survey.
[#Alam2017]_
The observable is based on the value of
the characteristic ratio :math:`r_s(z_d)/D_V(z)` between
the sound horizon :math:`r_s` at decoupling time (:math:`z_d`) and the
effective BAO distance :math:`D_V`, or some variation of this.
The respective references are given in the headers of the data files.

:math:`H(z)` data
^^^^^^^^^^^^^^^^^

These are the cosmic chronometer data.
30 measurements of the Hubble expansion rate :math:`H(z)` at redshifts between
0 and 2. [#Moresco2016]_
The values of redshift, :math:`H` and the uncertainties are given in the file
``Hz/Hz_Moresco_et_al_2016.txt``. 

:math:`H_0` data
^^^^^^^^^^^^^^^^

The :math:`2.4\%` precision local measure [#Riess2016]_ of :math:`H_0` is present in
``H0/local_H0_Riess_et_al_2016.txt``.

.. rubric:: Footnotes

.. [#Betoule2014] Betoule M. et al. "Improved cosmological constraints from a joint analysis of the SDSS-II and SNLS supernova samples". Astronomy & Astrophysics 568, A22 (2014).

.. [#Huang2015] Huang Q.-G., Wang K., Wang S. "Distance priors from Planck 2015 data". Journal of Cosmology and Astroparticle Physics 12 (2015) 022.

.. [#Chen2018] Chen L., Huang Q.~G., Wang K. "Distance Priors from Planck Final Release". arXiv:1808.05724v1 [astro-ph.CO].

.. [#Carter2018] Carter P. et al. "Low Redshift Baryon Acoustic Oscillation Measurement from the Reconstructed 6-degree Field Galaxy Survey". arXiv:1803.01746v1 [astro-ph.CO].

.. [#Anderson2014] Anderson L. et al. "The clustering of galaxies in the SDSS-III Baryon Oscillation Spectroscopic Survey: measuring :math:`D_A` and :math:`H` at :math:`z = 0.57` from the baryon acoustic peak in the Data Release 9 spectroscopic Galaxy sample". Monthly Notices of the Royal Astronomical Society 438 (2014) 83-101.

.. [#FontRibera2014] Font-Ribera A. et al. "Quasar-Lyman :math:`\alpha` forest cross-correlation from BOSS DR11: Baryon Acoustic Oscillations". Journal of Cosmology and Astroparticle Physics 05 (2014) 027.

.. [#Bautista2017] Bautista J. E. et al. "Measurement of baryon acoustic oscillation correlations at :math:`z = 2.3` with SDSS DR12 Ly :math:`\alpha`-Forests". Astronomy & Astrophysics 603 (2017) A12.

.. [#Kazin2014] Kazin E. A. et al. "The WiggleZ Dark Energy Survey: improved distance measurements to :math:`z = 1` with reconstruction of the baryonic acoustic feature". Monthly Notices of the Royal Astronomical Society 441 (2014) 3524-3542.

.. [#Alam2017] Alam S. et al. "The clustering of galaxies in the completed SDSS-III Baryon Oscillation Spectroscopic Survey: cosmological analysis of the DR12 galaxy sample". Monthly Notices of the Royal Astronomical Society 470 (2017) 2617-2652.

.. [#Moresco2016] Moresco M. et al. "A 6% measurement of the Hubble parameter at :math:`z \sim 0.45`: direct evidence of the epoch of cosmic re-acceleration". Journal of Cosmology and Astroparticle Physics 05 (2016) 014.

.. [#Riess2016] Riess A. G. et al. "A 2.4% determination of the local value of the Hubble constant". The Astrophysical Journal 826 (2016) 56.

