.. EPIC documentation master file, created by
   sphinx-quickstart on Mon Apr 03 22:35:50 2017.
   You can adapt this file completely to your liking, but it should at
   least contain the root `toctree` directive.


Welcome to the EPIC user's guide!
=================================

Easy Parameter Inference in Cosmology
(EPIC) is my implementation in Python of a MCMC code for
Bayesian inference of parameters of cosmological models and model comparison via
the computation of Bayesian evidences.


Details
-------

:Author: Rafael J. F. Marcondes

:Contact: rafaelmarcondes@usp.br

:Repository: https://bitbucket.org/rmarcondes/epic

:License: BSD License 2.0 with an added clause that if you use it in your work
          you must cite this user's guide published in the arXiv repository
          as: Marcondes R. J. F., "EPIC - Easy Parameter inference in
          Cosmology: The user's guide to the MCMC sampler". arXiv:1712.00263
          [astro-ph.IM]. See the ``LICENSE.txt`` file in the root directory of
          the source code for more details.

Contents
--------

.. toctree::
   :maxdepth: 2
   :numbered: 2

   welcome
   howtoinstall
   cosmology
   MCMCmodule
   acknowledgments

About the author
----------------

I'm a Brazilian engineer and physicist graduated from the Federal University of
Sao Carlos (Engineering Physics, 2009), the University of Campinas (M.Sc. in
Physics, 2012) and the University of Sao Paulo (Ph.D. in Physics, 2016).
Besides developing EPIC, I have worked mainly with tests of interacting dark
energy models using growth of structure and galaxy clusters data.

See the `inSPIRE-HEP <http://inspirehep.net/search?p=Rafael+Marcondes>`_
website to access my publications.

Changelog
---------

Version 1.4.?
^^^^^^^^^^^^^

New in this version (??? 2019)

* GUI now also shows the dark energy equation of state for the selected model.

Version 1.4.1
^^^^^^^^^^^^^

New in this version (October 2018):

* "Load" button becomes a menu button and now also gives an option to start a
  new simulation from a previously configured ``.ini`` file.
* Fix a bug with the command ``monitor``.
* Evolution of convergence is plotted after a MCMC simulation is finished.
* Other small changes.

Version 1.4
^^^^^^^^^^^

New in this version (September 2018):

* New EoS parametrization Jassal-Bagla-Padmanabhan (JBP).
* New datasets included: updated CMB distance priors from Planck 2018.
* New command ``burst`` to generate a triangle plot of the chains separately.
* You can now resume simulations from the GUI using the "Load" button.
* New button to empty list of solved models for distance comparison in the GUI.

Changed in this version:

* Renamed fast-varying EoS models to Linder-Huterer (LH) and
  Felice-Nesseris-Tsujikawa (FNT) models.
* Simulation output now displays total number of steps when resuming simulations
  (rather than the number of steps since last run).

Version 1.3.2
^^^^^^^^^^^^^

New in this version (September 2018):

* Fix logic regarding when background solutions should be recalculated in
  models that need numerical solution.
* New option ``--save-rejected`` to also write rejected states to disk.
* New option ``--use-chain`` to plot results of specific chains.
* Option ``--group-name`` is now ``--plot-prefix`` and is always applied with
  ``plot`` command, even when only one simulation is plotted.
* Other bug fixes.


Version 1.3.1
^^^^^^^^^^^^^

Changed in this version (July 2018):

* Bug fixes.

Version 1.3
^^^^^^^^^^^

New in this version (July 2018):

* You can now run MCMC from the graphical interface!
* EPIC creates a home folder for the ``*.ini`` configuration files, user
  modifications (new models, species, datasets, etc) at the users's home
  directory or other location set through the environment variable
  ``$EPIC_USER_PATH``.

Changed in this version:

* Simplified installation and setup process. When installing from PyPi, only
  the ``epic.py`` script and the relevant ``.ini`` files are exposed to the
  user in EPIC's home folder.
  Depending on your system configuration you will be able to run ``epic.py``
  from any directory.
* Other minor improvements.


Version 1.2.1
^^^^^^^^^^^^^

Changed in this version:

* Use gif images for button labels in GUI when TkVersion is inferior to 8.6.

Version 1.2
^^^^^^^^^^^

New in this version (July 2018):

* All the chains start at the same point now by default (use ``--multi-start``
  to make it behave as previously).
* Option to calculate (when plotting results with the command ``plot``) how
  many sigmas of detection for a given parameter when ``--kde`` is given (need
  to ``analyze`` with ``--kde`` first. If convergence is obtained within the
  required tolerance then ``--kde`` is included automatically).
* EPIC now brings a graphical user interface (GUI) for its cosmological
  calculator! Try it with the ``gui`` command. Check the new section in this
  user guide for instructions.
* Bug fixes and other minor improvements.

Version 1.1
^^^^^^^^^^^

New in this version (May 2018):

* A new module for Cosmology calculations written from scratch, following an
  intensively object-oriented approach. This update facilitates the use of this
  code for Cosmology calculations separated from a MCMC simulation. Try it
  interactively with jupyter notebook!
* Observational data sets and likelihood calculations have also been reworked
  and improved. Choice of observations and models and new model set up process
  should be more transparent now.
* Parallel Tempering algorithm has been removed in this version and may return
  as a new implementation in a future release.

Version 1.0.4
^^^^^^^^^^^^^

* Uses astropy.io.fits instead of pyfits for loading JLA (v4) files.

Version 1.0.2
^^^^^^^^^^^^^

New in this version:

* Included instructions in documentation on how to show two or more results in
  the same triangle plot.
* Added section "About the author" to documentation.
* This changelog.
* New background in html documentation favicon, uses readthedocs' color.
* Included arXiv eprint number of the PDF version of this documentation in
  license information.
* Slightly reduced mathjax fontsize in html documentation.
* Other minor changes to documentation.

Version 1.0.1
^^^^^^^^^^^^^

* First release on PyPi.
