
How to install
==============

You can obtain this program either from PyPi (installing with ``pip``) or cloning the public repository from BitBucket.
But first, it is recommended that you make these changes inside a
virtual environment.

Setting up a virtual environment
--------------------------------

On Unix systems
^^^^^^^^^^^^^^^

The preferred way is using Python3's ``venv`` module, available in Python 3.3
and superior versions.
When choosing this option on Unix systems, the main script ``epic.py`` will be
executable from any location.
In a directory of your preference, create a virtual Python environment (for
example, named ``EPIC-env``) and activate it with::

    $ python3 -m venv EPIC-env
    $ source EPIC-env/bin/activate

When you finish using the environment and want to leave it you can just use
``$ deactivate``. 
To activate it again, which you need in a new session, just run the activation
command above (the second line only).
More details about Python3's ``venv`` 
`here <https://docs.python.org/3/library/venv.html>`_.

Alternatively, you can install 
`pyenv and pyenv-virtualenv <https://github.com/pyenv>`_, which let you create
a virtual environment and even choose another version of Python to install.
This is done with::

    $ pyenv virtualenv 3.6.1 EPIC-env # or choose other version you like.
    $ pyenv activate EPIC-env # use 'pyenv deactivate' to deactivate it.

Note that this version of EPIC is not compatible with Python 2 anymore.

On Windows
^^^^^^^^^^

EPIC is supported on Windows through the Conda system.
Download and install `Miniconda3 <https://conda.io/miniconda.html>`_. 
Then, from the Anaconda prompt, create and activate your environment with::

    $ conda create -n EPIC-env python
    $ conda activate EPIC-env

Defining your custom EPIC home folder (optional)
------------------------------------------------

By default, data from the MCMC simulations will be saved to a folder named
``simulations`` in the same location of the input ``.ini`` file used in each
simulation.
You can have a common location for all results by defining an environment
variable. 
This makes sense since the output files can be big and you might want to store
them in a separate driver.

To do this, before installing EPIC, set the variable ``EPIC_USER_PATH`` to the
location of your preference.
On Unix systems, add the following line to your ``~/.bashrc`` or
``~/.bashprofile`` or ``~/.profile`` file::

    export EPIC_USER_PATH="/path/to/folder/"

so the variable will be defined in every session.
On Windows, open Control Panel, go to System and Security, System, Advanced
system settings, Environment Variables.
In the section User variables for (your user), click the New button, browse to
the directory that you want to set as EPIC's home and create the variable with
the name ``EPIC_USER_PATH``.
   

Installing EPIC
---------------

From PyPi
^^^^^^^^^

The easiest way to install this program is to get it from PyPi.
It is also the most recommended since it can be easily updated when new
versions come out.
Inside your virtual environment, run::

    $ pip install epic-code

You are now good to go.
During the installation, some ``.ini`` files will be extracted to the EPIC's home folder in your home folder or in ``EPIC_USER_PATH`` if you defined it before.
If you used ``venv`` and your system is Unix, you may be able to launch EPIC's
graphical interface just by running::

    $ epic.py

from any location. In other configurations, when this cannot be achieved, the
script ``epic.py`` will be exported to the home folder so you can execute it
from that location with::

    $ python epic.py

To check and install updates if available, just run::

    $ pip install --upgrade epic-code

from inside your environment.

Cloning the git repository
^^^^^^^^^^^^^^^^^^^^^^^^^^

If you plan to contribute to this program you can clone the git repository at
`<https://bitbucket.org/rmarcondes/epic>`_, running::

    $ git clone https://bitbucket.org/rmarcondes/epic.git

After downloading the repository, ``cd`` into the ``epic`` folder and install
the program with::

    $ pip install -e .

This should be repeated as you edit the program unless you are always running
it from the ``EPIC`` folder.
To use modified code from the python interactive interpreter you need to install
it again.

