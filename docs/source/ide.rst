
To create an instance of a coupled model (``cde``) with
:math:`Q \propto \rho_c`, use:

.. code:: ipython3

    >>> from EPIC.cosmology import cosmic_objects as cosmo
    
    >>> CDE = cosmo.CosmologicalSetup(
    ...     'cde',
    ...     interaction_setup={
    ...         'species': ['idm', 'ide'],
    ...         'parameter': {'idm': 'xi'},
    ...         'propto_other': {'ide': 'idm'},
    ...         'sign': {'idm':1, 'ide':-1},
    ...     },
    ...     physical=False,
    ...     derived='ide'
    ... )

The mandatory species are ``idm`` and ``ide``. You can add ``baryons``
in the ``optional_species`` list keyword argument, but note that
``matter`` is not available as a combined species for this model type
since dark matter is interacting with another fluid while baryons are
not. What is new here is the ``interaction_setup`` dictionary. This is
where we tell the code which ``species`` are interacting (at the moment
only an energy exchange within a pair is supported), to which of them
(``idm``) we associate the interaction ``parameter`` ``xi``, indicate
the second one (``ide``) as having an interaction term proportional to
the other (``idm``) and specify the sign of the interaction term for
each fluid, in this case that means :math:`Q_c = 3 H \xi \rho_c` and
:math:`Q_d = - 3 H \xi \rho_c`.

.. code:: ipython3

    >>> parameters = {'Oc0': 0.3, 'H0': 68, 'xi': 0.4, 'wd': -1}
    >>> CDE.solve_background(parameter_space=parameters)
    >>> show_densities(CDE, parameter_space=parameters)


.. image:: ide_files/ide_3_0.png


Here, I am exaggerating the value of the interaction parameter so we can
see a variation on the dark energy density that is due to the
interaction, not the equation-of-state parameter, which is :math:`-1`.
This same cosmology can be realized with the model type ``cde_lambda``
without specifying the parameter ``wd``, since the ``ilambda`` fluid has
fixed :math:`w_d = -1`. The dark matter interacting term :math:`Q_c` is
positive with :math:`\xi` positive, thus the lowering of the dark energy
density as its energy flows towards dark matter.

