
Easy Paramater Inference in Cosmology (EPIC)
============================================


A Markov Chain Monte Carlo program for parameter inference and model comparison in Cosmology.

2017, Rafael Marcondes.
